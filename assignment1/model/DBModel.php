<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Model implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            try
            {
            $this->db = new PDO('mysql:host=localhost; dbname=oblig_1; charset=utf8', 'root', ''); // Connect to database
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e)
            {
                $view = new ErrorView("Error message: unable to connect to database");
                $view->create();
                exit(); // Since there is no connection exit script
            
            }
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        try
        {
            $stmt = $this->db->query('SELECT * FROM book ORDER BY id');
            while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
            }
        }
        catch(PDOException $e)
        {
            $view = new ErrorView("Error message: Could not get book list from database");
            $view->create();
            exit();
        }
        return $booklist;
    }
   
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
        try
        {
            $stmt = $this->db->query("SELECT * FROM book WHERE id = $id");
            $stmt->execute();
            $row = $stmt->fetch();
            $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
            
        }
        catch(PDOException $e)
        {
            $view = new ErrorView("Error message: unable to find book with input id");
            $view->create();
            exit();
        }
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        // Input check
        if(empty($book->title && $book->author)){ // Check if title and author input is empty
            $view = new ErrorView("Error message: Fill in the title and author boxes");
            $view->create();
            exit(); // Stop exectuing script
        }

        try
        {
            $stmt = $this->db->prepare("INSERT INTO book (title, author, description) VALUES (:title, :author, :description)");  // Prepare a statement
            $stmt->bindValue(':title', $book->title);
            $stmt->bindValue(':author', $book->author);
            $stmt->bindValue(':description', $book->description);
            $stmt->execute();
            
        }
        catch(PDOException $e)
        {
            $view = new ErrorView("Error message: unable to add a book to database");
            $view->create();
            exit();
        }            
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if(empty($book->title && $book->author)){ // Check if title and author input is empty
            $view = new ErrorView("Error message: Fill in the title and author boxes");
            $view->create();
            exit(); // Stop exectuing script
        }
        try
        {
            $stmt = $this->db->prepare("UPDATE book set title = :title, author = :author, description = :description WHERE id = :id");
            $stmt->bindValue(':title', $book->title);
            $stmt->bindValue(':author', $book->author);
            $stmt->bindValue(':description', $book->description);
            $stmt->bindValue(':id', $book->id);
            $stmt->execute();
        }
        catch(PDOException $e)
        {
             $view = new ErrorView("Error message: unable to modify book in database");
            $view->create();
            exit();
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        try
        {
            $stmt = $this->db->prepare("DELETE FROM book WHERE id = '$id'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            $view = new ErrorView("Error message: unable to delete book from database");
            $view->create();
            exit();
        }
    }
        
        
	
}

?>